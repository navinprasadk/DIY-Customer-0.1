var express = require("express");
var router = express.Router();
let ingredientController = require("../controller/IngredientController");

router.get("/ingredients", ingredientController.getIngredientDetails);
router.get("/checkUrl", ingredientController.checkUrl);

module.exports = router;
