var mongoose = require("mongoose");
var Schema = mongoose.Schema;
let ingredientSchema = new Schema({
  url: String,
  procedure: String,
  EssentialIngredient: [
    {
      name: String
    }
  ],
  MainIngredient: [
    {
      name: String,
      quantity: String,
      unit: String
    }
  ]
});

module.exports = mongoose.model("details", ingredientSchema);
