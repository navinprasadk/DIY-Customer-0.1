let ingredientSchema = require("./../models/IngredientSchema.js");

module.exports = {
  checkUrl: function(req, res) {
    var url = req.query.url;
    console.log("url:" + url);
    ingredientSchema.find({ url: url }, function(err, details) {
      if (err) {
        res.send(err);
      } else {
        console.log(details);
        res.send(details);
      }
    });
  },
  getIngredientDetails: function(req, res) {
    var url = req.query.url;
    console.log("url:" + url);
    ingredientSchema.find({ url: url }, function(err, details) {
      if (err) {
        res.send(err);
      } else {
        console.log(details);
        res.send(details);
      }
    });
  }
};
