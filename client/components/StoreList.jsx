import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import $ from "jquery";
import {
  Dropdown,
  Menu,
  Label,
  Card,
  Form,
  Button,
  Grid,
  Input,
  Message
} from "semantic-ui-react";
import { CountryDropdown, RegionDropdown } from "react-country-region-selector";
import "../styles/StoreList.css";
import Header from "./header.jsx";

const countryOptions = [
  {
    key: "United states",
    value: "United states",
    flag: "us",
    text: "United states"
  }
];
const stateOptions = [
  { key: "Alaska", value: "Alaska", text: "Alaska" },
  { key: "California", value: "California", text: "California" },
  { key: "Florida", value: "Florida", text: "Florida" },
  { key: "Georgia", value: "Georgia", text: "Georgia" },
  { key: "Hawaii", value: "Hawaii", text: "Hawaii" },
  { key: "New Jersey", value: "New Jersey", text: "New Jersey" },
  { key: "Ohio", value: "Ohio", text: "Ohio" },
  { key: "Texas", value: "Texas", text: "Texas" },
  { key: "Washington", value: "Washington", text: "Washington" },
  { key: "Virginia", value: "Virginia", text: "Virginia" }
];
const storeOptions = [
  { key: "John's Store", value: "John's Store", text: "John's Store" },
  {
    key: "Papa's John Store",
    value: "Papa's John Store",
    text: "Papa's John Store"
  }
];

export default class StoreList extends Component {
  constructor() {
    super();
    this.state = {
      storeStatus: true,
      buttonStatus: true,
      url: "",
      linkStatus: true,
      showMessage: false,
      submitBtn: false,
      store: ""
    };
  }
  /*To enable store dropdown if state dropdown is selected*/
  regionChange() {
    this.setState({ storeStatus: false });
  }

  /*To enable proceed button if store dropdown is selected*/
  storeChange(e, data) {
    this.setState({ linkStatus: false, store: data.value });
  }

  handleUrlChange(e) {
    this.setState({ buttonStatus: false, url: e.target.value });
  }

  onSubmitBtn() {
    var that = this;
    $.ajax({
      type: "GET",
      url: "/checkUrl",
      data: { url: that.state.url },
      success: function(data) {
        console.log("data", data);
        if (data != "") {
          that.setState({ submitBtn: true });
        } else {
          console.log("no data");
          that.setState({ submitBtn: false, showMessage: true });
        }
      },
      error: function(err) {
        that.setState({ buttonStatus: false, showMessage: true });
      }
    });
  }
  render() {
    return (
      <div className="background-image">
      <div className="App">
        <Header />
        <Grid stackable>
          <Card className="storeListCard" raised style={{ marginTop: "6%", marginLeft: "72%", border:"4px solid #FF7701",backgroundColor: "#333F50"}}>
            <Card.Header
              style={{
                fontFamily: "Sitka Banner",
                fontSize: "24",
                marginTop: "5%",
                fontWeight: "bold",
                color:"#FBE5D6"
              }}
            >
              Fill Your Details
            </Card.Header>
            <Form>
              <Grid.Row style={{ margin: "6%" }}>
                <Grid.Column>
                  <Button
                    content="United states"
                    icon="globe"
                    labelPosition="left"
                    style={{
                      border: "1px solid black",
                      width: "90%",
                      fontFamily: "Sitka Banner",
                      fontSize: "18"
                    }}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{ margin: "6%" }}>
                <Grid.Column>
                  <Dropdown
                    button
                    className="icon "
                    floating
                    labeled
                    icon="circle outline"
                    options={stateOptions}
                    search
                    placeholder="Select a State"
                    style={{
                      border: "1px solid black",
                      width: "90%",
                      textAlign: "center",
                      fontFamily: "Sitka Banner",
                      fontSize: "18"
                    }}
                    onChange={this.regionChange.bind(this)}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{ margin: "6%" }}>
                <Grid.Column>
                  <Dropdown
                    button
                    className="icon "
                    floating
                    labeled
                    icon="shopping basket"
                    options={storeOptions}
                    search
                    placeholder="Select a Store"
                    style={{
                      border: "1px solid black",
                      width: "90%",
                      textAlign: "center",
                      fontFamily: "Sitka Banner",
                      fontSize: "18"
                    }}
                    disabled={this.state.storeStatus}
                    onChange={this.storeChange.bind(this)}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row style={{ margin: "6%" }}>
                <Grid.Column>
                  <Input
                    button
                    className="Icon urlInput"
                    labeled
                    className="icon "
                    icon="linkify"
                    // label='http://'
                    placeholder="YouTube link"
                    iconPosition="left"
                    onChange={this.handleUrlChange.bind(this)}
                    disabled={this.state.linkStatus}
                    style={{
                      border: "1px solid black",
                      borderRadius: "5%",
                      width: "90%",
                      textAlign: "center",
                      fontFamily: "Sitka Banner",
                      fontSize: "18"
                    }}
                  />{" "}
                  {this.state.showMessage ? (
                    <Label basic color="red" pointing>
                      Please enter a valid URL
                    </Label>
                  ) : (
                    ""
                  )}
                </Grid.Column>
              </Grid.Row>
              {/* {this.state.showMessage?<Grid.Row style={{ margin: "10%" }}>
                <Grid.Column>
                  <Message negative style={{padding:'4%'}}>
                    <Message.Header>Entered Invalid URL</Message.Header>
                  </Message>
                </Grid.Column>
              </Grid.Row>:''} */}
              <Grid.Row style={{ margin: "10%" }}>
                <Grid.Column>
                  {this.state.buttonStatus ? (
                    <Button  color="orange"
                      style={{
                        marginLeft: "40%",
                        fontFamily: "Sitka Banner",
                        fontSize: "18"
                      }}

                      type="submit"
                      disabled={this.state.buttonStatus}
                      onClick={this.onSubmitBtn.bind(this)}
                    >
                      {" "}
                      Proceed
                    </Button>
                  ) : (
                    <Button color="orange"
                      style={{
                        marginLeft: "40%",
                        fontFamily: "Sitka Banner",
                        fontSize: "18"
                      }}
                      type="submit"
                      onClick={this.onSubmitBtn.bind(this)}
                    >
                      {" "}
                      Proceed
                    </Button>
                  )}
                </Grid.Column>
              </Grid.Row>
            </Form>
            {this.state.submitBtn ? (
              <Redirect
                to={{
                  pathname: "/ingredients",
                  data: { url: this.state.url, store: this.state.store }
                }}
              />
            ) : (
              ""
            )}
          </Card>
        </Grid>
      </div>
    </div>
    );
  }
}
