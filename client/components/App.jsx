import React, { Component } from "react";
import StoreList from "./StoreList.jsx";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <StoreList />
      </div>
    );
  }
}
