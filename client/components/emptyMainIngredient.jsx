import React, { Component } from "react";
import { Header, Icon } from "semantic-ui-react";
export default class EmptyMainIngredient extends Component {
  render() {
    return (
      <div>
        <Header as="h3" icon textAlign="center">
          <Icon name="shopping basket" />
          No Ingredients
        </Header>
      </div>
    );
  }
}
