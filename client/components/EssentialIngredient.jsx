import React, { Component } from "react";
import { List, Label, Tab, Table, Icon, Modal } from "semantic-ui-react";

export default class EssentialIngredient extends Component {
  render() {
    console.log(this.props.essentialIngredient);
    return (
      <div>
        <Modal
          trigger={
            <Table style={{backgroundColor:'#333F50'}}>
            <Table.Cell style={{paddingLeft:'0%'}}>
              <a style={{color:'#FBE5D6'}}>{this.props.essentialIngredient}</a>
            </Table.Cell>
            </Table>
          }
          closeIcon
        >
          <Modal.Header>e-cart </Modal.Header>
          <Modal.Content image scrolling />
        </Modal>
      </div>
    );
  }
}
