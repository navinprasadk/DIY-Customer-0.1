import React from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import $ from "jquery";
import "../styles/StoreList.css";
import {
  List,
  Label,
  Tab,
  Table,
  Icon,
  Modal,
  Grid,
  Image,
  Segment,
  Button,
  Header
} from "semantic-ui-react";
import Navbar from "./header.jsx";
import MainIngredientList from "./MainIngredientList.jsx";
import EssentialIngredientList from "./EssentialIngredient.jsx";
import EmptyMainIngredient from "./emptyMainIngredient.jsx";
import EmptyEssentialIngredient from "./emptyEssentialIngredient.jsx";
import { Scrollbars } from "react-custom-scrollbars";

class Ingredients extends React.Component {
  constructor() {
    super();
    this.state = {
      procedure: "",
      mainIngredient: "",
      essentialIngredient: "",
      height1: "",
      height2: "",
      displayEssential: false,
      displayMain: false,
      displayDoneBtn: false,
      title: ""
    };
  }

  componentDidMount() {
    this.setState({ height1: window.innerHeight - 400 + "px" });
    this.setState({ height2: window.innerHeight - 370 + "px" });
  }
  componentWillMount() {
    var that = this;
    console.log("url:" + that.props.location.data.url);
    $.ajax({
      type: "GET",
      url: "/ingredients",
      data: { url: that.props.location.data.url },
      success: function(data) {
        console.log(data);

        if (data[0].MainIngredient == "" && data[0].EssentialIngredient == "") {
          that.setState({ displayDoneBtn: false });
        } else {
          that.setState({ displayDoneBtn: true });
        }
        if (data[0].title != "") {
          that.setState({ title: data[0].title });
        } else {
          that.setState({ title: "" });
        }
        if (data[0].procedure != "") {
          that.setState({ procedure: data[0].procedure });
        } else {
          that.setState({ procedure: "No procedure" });
        }
        if (data[0].MainIngredient != "") {
          that.setState({ displayMain: true });
          var mainIngredient = data[0].MainIngredient;
          var mainIngredientList = mainIngredient.map((row, index) => {
            return (
              <MainIngredientList
                ingredients={row.name}
                quantity={row.quantity}
                unit={row.unit}
                key={index}
              />
            );
          });
        } else {
          that.setState({ displayMain: false });
        }
        if (data[0].EssentialIngredient != "") {
          that.setState({ displayEssential: true });
          var essentialIngredient = data[0].EssentialIngredient;
          var essentialIngredientList = essentialIngredient.map(
            (row, index) => {
              return (
                <EssentialIngredientList
                  essentialIngredient={row.name}
                  key={index}
                />
              );
            }
          );
        } else {
          that.setState({ displayEssential: false });
        }
        that.setState({
          mainIngredient: mainIngredientList,
          essentialIngredient: essentialIngredientList
        });
      },
      error: function(err) {
        alert("error");
      }
    });
  }
  render() {
    if (this.state.mainIngredient != "") {
      var dd = this.state.mainIngredient.map(item => {
        return (
          <Table.Row style={{backgroundColor:"#333F50"}}>
            <Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              <Modal
                trigger={<a className="anchor">{item.props.ingredients}</a>}
                closeIcon
              >
                <Modal.Content>
                  <p style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
                    ors
                  </p>
                </Modal.Content>
              </Modal>
            </Table.Cell>
            <Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              {item.props.quantity}
            </Table.Cell>
            <Table.Cell style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
              {item.props.unit}
            </Table.Cell>
          </Table.Row>
        );
      });
    } else {
      var dd = (
        <div style={{ fontFamily: "Poor Richard", fontSize: "20" }}>
          It has no ingredients
        </div>
      );
    }
    const panes = [
      {
        menuItem: "MAIN INGREDIENTS",
        pane: (
          <Tab.Pane key="tab1" style={{backgroundColor:'#333F50',fontFamily: "Poor Richard", border:"3px solid #FF7701"
}}>
            {/* <p>This tab has ingredients which you can add to cart.</p> */}
            {this.state.displayMain ? (
              <Scrollbars style={{ height: this.state.height1 }}>
                <Table color='#333F50' inverted padded>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color:'#FBE5D6'
                        }}
                      >
                        Ingredients
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color:'#FBE5D6'
                        }}
                      >
                        Quantity
                      </Table.HeaderCell>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color:'#FBE5D6'
                        }}
                      >
                        Units
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body
                    style={{ fontFamily: "Poor Richard", fontSize: "20" ,color:'#FBE5D6'}}
                  >
                    {dd}
                  </Table.Body>
                </Table>
              </Scrollbars>
            ) : (
              <EmptyMainIngredient />
            )}
          </Tab.Pane>
        )
      },
      {
        menuItem: "ESSENTIAL INGREDIENTS",
        pane: (
          <Tab.Pane key="tab2" style={{backgroundColor:'#333F50',fontFamily: "Poor Richard",                   border:"3px solid #FF7701"
}}>
            {/* <p>This tab has a additional ingredients which you can add to cart.</p> */}
            {this.state.displayEssential ? (
              <Scrollbars style={{ height: this.state.height1 }}>
                <Table color='#333F50' inverted padded>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell
                        style={{
                          backgroundColor: "#333F50",
                          fontFamily: "Poor Richard",
                          fontSize: "20",
                          color:'#FBE5D6'
                        }}
                      >
                        Item
                      </Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body
                    style={{ fontFamily: "Poor Richard", fontSize: "20", color:'#FBE5D6' }}
                  >
                    <Table.Row>
                      <Table.Cell
                        style={{ fontFamily: "Poor Richard", fontSize: "20", color:'#FBE5D6', backgroundColor:'#333F50' }}
                      >
                        {/* <Modal
                          trigger={ */}
                            {/* <a> */}
                              {this.state.essentialIngredient}
                            {/* </a> */}
                        {/* //   }
                        //   closeIcon
                        // > */}
                          {/* <Modal.Content>
                            <p
                              style={{
                                fontFamily: "Poor Richard",
                                fontSize: "20"
                              }}
                            >
                              ors
                            </p>
                          </Modal.Content>
                        </Modal> */}
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
              </Scrollbars>
            ) : (
              <EmptyEssentialIngredient />
            )}
          </Tab.Pane>
        )
      }
    ];

    return (
      <div className="Ingredients">
        <Navbar />
        <div>
          {/* <Tab  panes={panes} renderActiveOnly={false} />; */}
          <Header
            as="h2"
            style={{
              color: "white",
              textAlign: "center",
              textTransform: "uppercase",
              fontFamily: "Sitka Banner",
              fontSize: "24",
              color:'#333f50'
            }}
          >
            {" "}
            {this.state.title}{" "}
          </Header>
          <Grid columns={2}>
            <Grid.Column
              style={{
                paddingLeft: "3%",
                fontFamily: "Poor Richard",
                fontSize: "20"
              }}
            >
              <Segment
                style={{
                  fontFamily: "Poor Richard",
                  fontSize: "20",
                  textAlign: "justify",
                  backgroundColor:'#333F50',
                  color:'#FBE5D6',
                  border:"3px solid #FF7701"
                }}
              >
                The below list shows the quantity according to the suggestion by
                the blogger. Kindly edit the quantity in retailer's site
                according to the need.
              </Segment>

              <Tab
                // menu={{ color:"black", inverted: true}}
                panes={panes}
                renderActiveOnly={false}
                style={{
                  fontFamily: "Poor Richard",
                  fontSize: "20",
                  textAlign: "justify",
                }}
              />
            </Grid.Column>
            <Grid.Column
              style={{
                paddingRight: "3%",
                fontFamily: "Poor Richard",
                fontSize: "20"
              }}
            >
              <Header
                as="h3"
                title="procedure"
                style={{
                  marginTop: "18%",
                  fontFamily: "Poor Richard",
                  fontSize: "20",
                  color:'#333f50'
                }}
                inverted
              >
                PROCEDURE
              </Header>
              {/* <Label as='a' color='black' style={{marginLeft:'3%'}} ribbon>Procedure</Label> */}
              {/*}  <Segment><Header as='h3'  content='Procedure' icon='bars'/></Segment>*/}
              <Scrollbars style={{ height: this.state.height2 ,              border:"3px solid #FF7701"
}}>
                <Segment raised style={{backgroundColor:"#333f50"}}>
                  <p
                    style={{
                      textAlign: "justify",
                      padding: "2%",
                      fontFamily: "Poor Richard",
                      fontSize: "20",
                      color:'#FBE5D6'
                    }}
                  >
                    {this.state.procedure}
                  </p>
                </Segment>
              </Scrollbars>
            </Grid.Column>
            {this.state.displayDoneBtn ? (
              <Link  color="orange"
                to={{
                  pathname: "/done",
                  store: this.props.location.data.store
                }}
              >
                <Button  color="orange"
                  style={{
                    marginLeft: "30%",
                    fontFamily: "Sitka Banner",
                    fontSize: "18"
                  }}
                >
                  Done
                </Button>
              </Link>
            ) : (
              ""
            )}
          </Grid>
        </div>
      </div>
    );
  }
}

export default Ingredients;
